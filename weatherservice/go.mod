module zeny.uk/epam

go 1.16

require (
	github.com/gomodule/redigo v1.8.5
	github.com/gorilla/mux v1.8.0
	github.com/rs/zerolog v1.25.0
)
