package model

type WeatherResult struct {
	WindSpeed int `json:"wind_speed"`
	TemperatureDegrees int `json:"temperature_degrees"`
}