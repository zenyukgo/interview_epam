package businesslogic

import (
	"context"
	"errors"
	"fmt"

	"github.com/gomodule/redigo/redis"
	"github.com/rs/zerolog/log"
	"zeny.uk/epam/internal/cache"
	"zeny.uk/epam/internal/model"
	"zeny.uk/epam/internal/providers/open_weather_map"
	"zeny.uk/epam/internal/providers/weather_stack"
)

// cacheDurationLimit - for how many seconds a record stored in cache should be counted as actual
const cacheDurationLimit = 3

type weatherBusinessLogic struct {
	weatherCache cache.WeatherCache
}

// WeatherBusinessLogic - interface to call the Business Logic layer
type WeatherBusinessLogic interface {
	GetWeatherByCity(ctx context.Context, city string) (*model.WeatherResult, error)
}

func NewWeatherBusinessLogic(redisConn redis.Conn) WeatherBusinessLogic {
	return &weatherBusinessLogic{cache.NewCache(redisConn)}
}

// GetWeatherByCity - returns a Weather entity
func (bl *weatherBusinessLogic) GetWeatherByCity(ctx context.Context, city string) (*model.WeatherResult, error) {
	_, contextIsAlive := ctx.Deadline()
	if !contextIsAlive {
		return nil, errors.New("context timed out")
	}

	// first try to get from the cache
	weather, exist, secondsAgo, err := bl.weatherCache.GetWeatherByCity(city)
	if err != nil {
		return nil,  fmt.Errorf("businesslogic - GetWeatherByCity - error while trying to get weather by city from cache; details: %v", err)
	}
	if exist && weather != nil && secondsAgo <= cacheDurationLimit{
		return weather, nil
	}

	weather, err = bl.tryAllProviders(city)
	if err != nil {
		return nil, fmt.Errorf("error getting weather record from providers; details: %v", err)
	}

	return weather, nil
}

func (bl *weatherBusinessLogic) tryAllProviders(city string) (*model.WeatherResult, error) {
	var weather = new(model.WeatherResult)
	var err error
	weather, err = bl.getWeatherFromPrimaryProvider(city)
	if err != nil {
		weather, err = bl.getWeatherFromSecondaryProvider(city)
		if err != nil {
			log.Err(errors.New("all weather providers have failed"))
			// read from cache
			var exist bool
			weather, exist, _, err = bl.weatherCache.GetWeatherByCity(city)
			if exist && err == nil && weather != nil {
				log.Printf("returning outdated weather record for city: %s", city)
				return weather, nil
			}
			if err != nil {
				return nil, fmt.Errorf("error getting weather for city: %s from local storage and all providers, details: %s", city, err)
			}
			if weather == nil {
				return nil, fmt.Errorf("error getting weather for city: %s from local storage and all providers, details: %s", city, err)
			}
			if !exist {
				return nil, fmt.Errorf("can not get weather for city: %s information doesn't exist neither locally nor at any supported provider", city)
			}
		}
	}
	return weather, nil
}

// getting from Weatherstack API
func (bl *weatherBusinessLogic) getWeatherFromPrimaryProvider(city string) (*model.WeatherResult, error){
	weather, err := weather_stack.GetWeatherByCity(city)
	if err != nil {
		return nil, fmt.Errorf("error while trying to get weather from WeatherStack API, details: %v", err)
	}
	log.Printf("received a new weather record from WeatherStack: %v", weather)

	// store in cache
	err = bl.weatherCache.StoreWeatherForCity(city, weather)
	if err != nil {
		log.Err(fmt.Errorf("error while trying to store weather record in cache; details: %v", err))
	}

	return weather, nil
}

// getting from OpenWeatherMap API
func (bl *weatherBusinessLogic) getWeatherFromSecondaryProvider(city string) (*model.WeatherResult, error){
	weather, err := open_weather_map.GetWeatherByCity(city)
	if err != nil {
		return nil, fmt.Errorf("error while trying to get weather from OpenWeatherMap API, details: %v", err)
	}
	log.Printf("received a new weather record from OpenWeatherMap: %v", weather)

	// store in cache
	err = bl.weatherCache.StoreWeatherForCity(city, weather)
	if err != nil {
		log.Error().Msgf("error while trying to store weather record in cache; details: %v", err)
	}

	return weather, nil
}