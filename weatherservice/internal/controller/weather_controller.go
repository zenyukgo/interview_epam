package controller

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"github.com/gomodule/redigo/redis"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"zeny.uk/epam/internal/businesslogic"
)

// NewWeatherController - instantiates a weather controller
func NewWeatherController(redisConn redis.Conn) *WeatherController {
	bl := businesslogic.NewWeatherBusinessLogic(redisConn)
	return &WeatherController{bl}
}

type WeatherController struct {
	weatherBusinessLogic businesslogic.WeatherBusinessLogic
}

func (c *WeatherController) GetWeatherByCity(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(r.Context(), 30*time.Second)
	defer cancel()

	httpParams := mux.Vars(r)
	city := strings.ToLower(httpParams["city"])
	if city == "" || len(city) > 100 {
		http.Error(w, "invalid request parameter - city", 400)
		return
	}

	weather, err := c.weatherBusinessLogic.GetWeatherByCity(ctx, city)
	if err != nil {
		log.Err(err)
		http.Error(w, "internal error", 500)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.Encode(weather)
}

