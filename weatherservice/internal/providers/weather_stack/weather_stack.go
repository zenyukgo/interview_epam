package weather_stack

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"zeny.uk/epam/internal/model"
)

// TODO: move keys to storage for secrets
const accessKey = "6d02ec8a8f417784a6bafacbbedd22da"

func GetWeatherByCity(city string) (*model.WeatherResult, error) {
	var apiUrl = "http://api.weatherstack.com/current?access_key=" + accessKey + "&query=" + city
	resp, err := http.Get(apiUrl)
	if err != nil {
		return nil, fmt.Errorf("error getting weather from Weatherstack for %s; details: %v", city, err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading weather from Weatherstack response, city: %s; details: %v", city, err)
	}

	result, err := convertToWeatherResult(body)
	if err != nil {
		return nil, fmt.Errorf("error converting Weatherstack response, city: %s; details: %v", city, err)
	}

	return result, nil
}

