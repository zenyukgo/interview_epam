package weather_stack

import "testing"

func TestConvertToWeatherResult(t *testing.T) {
	// arrange
	expectedTemperature := 29
	expectedWindSpeed := 30
	rawResponse := []byte(`{"request":{"type":"City","query":"Sydney, Australia","language":"en","unit":"m"}, "location":{"name":"Sydney","country":"Australia","region":"New South Wales","lat":"-33.883","lon":"151.217","timezone_id":"Australia\/Sydney","localtime":"2021-09-11 15:24","localtime_epoch":1631373840,"utc_offset":"10.0"},
		"current":{"observation_time":"05:24 AM",
			"temperature":29,"weather_code":113,"weather_icons":["https:\/\/assets.weatherstack.com\/images\/wsymbols01_png_64\/wsymbol_0001_sunny.png"],"weather_descriptions":["Sunny"],
			"wind_speed":30, "wind_degree":290,"wind_dir":"WNW","pressure":1010,"precip":0,"humidity":12,"cloudcover":0,"feelslike":28,"uv_index":6,"visibility":10,"is_day":"yes"}}`)

	// act
	actual, err := convertToWeatherResult(rawResponse)

	// assert
	if err != nil {
		t.Fatal(err)
	}
	if actual.WindSpeed != expectedWindSpeed {
		t.Fatal("wind speed doesn't match")
	}
	if	actual.TemperatureDegrees != expectedTemperature {
		t.Fatal("temperature doesn't match")
	}
}
