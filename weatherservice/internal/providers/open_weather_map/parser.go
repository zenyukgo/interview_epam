package open_weather_map

import (
	"encoding/json"
	"fmt"
	"math"

	"zeny.uk/epam/internal/model"
)

type openWeatherMapResult struct {
	Main openWeatherMapResultMain `json:"main"`
	Wind openWeatherMapResultWind `json:"wind"`
}

type openWeatherMapResultMain struct {
	Temperature float64 `json:"temp"`
}

type openWeatherMapResultWind struct {
	Speed float64 `json:"speed"`
}

func convertToWeatherResult(raw []byte) (*model.WeatherResult, error) {
	parsedResponse := openWeatherMapResult{}
	err := json.Unmarshal(raw, &parsedResponse)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling response, details: %v", err)
	}
	result := &model.WeatherResult{TemperatureDegrees: int(math.Round(parsedResponse.Main.Temperature)), WindSpeed: int(math.Round(parsedResponse.Wind.Speed))}
	return result, nil
}
