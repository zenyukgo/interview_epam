package open_weather_map

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"zeny.uk/epam/internal/model"
)

// TODO: move keys to storage for secrets
const apiKey = "26f0aaf2d49442badace152b874efff0"

func GetWeatherByCity(city string) (*model.WeatherResult, error) {
	//TODO: specify country, e.g.: sydney,AU
	var apiUrl = "http://api.openweathermap.org/data/2.5/weather?q=" + city +"&units=metric&appid=" + apiKey

	resp, err := http.Get(apiUrl)
	if err != nil {
		return nil, fmt.Errorf("error getting weather from OpenWeatherMap for %s; details: %v", city, err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading weather from OpenWeatherMap response, city: %s; details: %v", city, err)
	}

	result, err := convertToWeatherResult(body)
	if err != nil {
		return nil, fmt.Errorf("error converting OpenWeatherMap response, city: %s; details: %v", city, err)
	}

	return result, nil
}
