package cache

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/gomodule/redigo/redis"
	"github.com/rs/zerolog/log"
	"zeny.uk/epam/internal/model"
)

type cache struct {
	connection redis.Conn
}

type WeatherCache interface {
	GetWeatherByCity(city string) (*model.WeatherResult, bool, int, error)
	StoreWeatherForCity(city string, weather *model.WeatherResult) error
}

func NewCache(conn redis.Conn) *cache{
	return &cache{conn}
}

// GetWeatherByCity - returns weather record from local cache
// parameters:
// *model.WeatherResult - weather record
// bool - true if record exists
// int - how many seconds ago the record was stored in the cache
// error
func (c *cache)GetWeatherByCity(city string) (*model.WeatherResult, bool, int, error) {
	values, err := redis.Strings(c.connection.Do("HMGET", "city:"+city, "wind_speed", "temperature", "time"))
	if err != nil {
		return nil, false, 0, errors.New("can NOT get weather record from cache")
	}
	if values == nil || len(values) < 1 || len(values[0]) < 1 {
		return nil, false, 0, nil
	}

	var weather model.WeatherResult
	weather.WindSpeed, err = strconv.Atoi(values[0])
	if err != nil {
		return nil, false, 0, fmt.Errorf("can't parse wind speed when reading from cache; city: %s", city)
	}
	weather.TemperatureDegrees, err = strconv.Atoi(values[1])
	if err != nil {
		return nil, false, 0, fmt.Errorf("can't parse temperature when reading from cache; city: %s", city)
	}
	storedAtUnixTime, err := strconv.ParseInt(values[2], 10, 64)
	if err != nil {
		return nil, false, 0, fmt.Errorf("can't parse time when reading from cache; city: %s", city)
	}
	secondsSinceStore := int(time.Now().Unix() - storedAtUnixTime)

	return &weather, true, secondsSinceStore, nil
}

func (c *cache)StoreWeatherForCity(city string, weather *model.WeatherResult) error {
	_, err := c.connection.Do("HMSET", "city:"+city,
		"wind_speed", weather.WindSpeed,
		"temperature", weather.TemperatureDegrees,
		"time", time.Now().Unix())
	if err != nil {
		log.Err(err)
		return errors.New("can NOT store weather record in redis cache")
	}

	return nil
}