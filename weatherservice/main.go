package main

import (
	"errors"
	"net/http"

	"github.com/gomodule/redigo/redis"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"zeny.uk/epam/internal/controller"

	"github.com/gorilla/mux"
)

const (
	httpPort  = ":8080"
	redisPort = ":6379"
)

func main() {
	log.Level(zerolog.DebugLevel)

	redisConnection, err := redis.Dial("tcp", "rediscache"+redisPort)
	if err != nil {
		log.Err(errors.New("Can NOT establish connection to Redis for caching purposes. Exiting. "))
	}
	defer redisConnection.Close()
	weatherController := controller.NewWeatherController(redisConnection)

	router := mux.NewRouter()
	router.HandleFunc("/v1/weather", weatherController.GetWeatherByCity).
		Queries("city", "{city}").
		Methods("GET")

	log.Info().Msgf("Web API starting on port ", httpPort)
	http.ListenAndServe(httpPort, router)
}
