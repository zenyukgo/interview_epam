# interview_epam



## Getting started

To make it easy for you to get started with this weather service, here's a list of recommended next steps.

## Requirements

Docker and docker-compose must be installed. 

## Running

There are two docker containers: the Weather service and a Redis server. To run the
solution, you need to navigate the root directory (interview_epam) and execute command: 
`docker-compose up` 

## Testing

Use your browser to nagivate to url: 
`http://localhost:8080/v1/weather?city=Sydney`

In return you should get a json response like:
```
{
"wind_speed": 20, "temperature_degrees": 29
}
```
